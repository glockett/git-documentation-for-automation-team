# <center><a id="app"></a>Git FAQ and Guide</center>


Created: April 17, 2019 
Author: Gabriel Lockett @glockett, Collabralink, Technologies, Inc. 

## Description
MACPro Project initiative to provide a guide to using Git and Git commands with relevance to the MACPro project tailored to an audience with little to no familiarity with Git or 'Git-like' repositories.

## Table of Contents

[Introduction](#introduction)

[How to use this Guide](#how-to-use-this-guide)

[Quick Reference](#quick-references)

[Getting Started](#getting-started)
-- Overview
-- Creating new automation Driver workflow	

[Branching Strategy using Git](#branching-strategy-using-git)
-- Overview

1. [Setting up Local Repository	]()

2. [Branching]()

3. [Branch Scope]()

4. [Creating Branches (Local and Remote)]()

5. [Deleting Branches (Local and remote)]()

6. [Branch Tag]()

[Git Workflow](#git-workflow)

[Use Cases](#use-cases)
* [Syncing Changes with Team]()

[Appendix](#appendix)

***
## Introduction

>>>
Git (/ɡɪt/) is a distributed version-control system for tracking changes in source code during software development.[^1]
It is designed to address the limitations of other traditional version control methods (such as Mercurial, SVK, SVN, BitKeeper, etc.). 

[^1]: [Wikipedia](https://en.wikipedia.org/wiki/Git) "Wikipedia"
>>>
Key terms and features will be annotated throughout this document, with links to the definitions and/or further explanation. 




Below are some key features of Git:

*KEY FEATURES:*
-	**Non-linear development** – Developers can create “branches” off of the master branch of the source code repository, developing features independent 
of other team members. 
-	**Distributed development** – Developer flexibility by “cloning” the remote Git repository to a local Git instance on their workstation. Then, they can 
freely develop source code within their local repository.
-	**Compatibility** – Repositories are published and changes can be “pushed” via HTTPS, SSH, SFTP, and rsync.
-	**Efficiency** – Branches are fast, scalable and light-weight; repository is only “cloned” once. Afterwards, delta changes and associated metadata 
are “pulled” into the local repository.
-	**Data integrity** – All Git history and version control changes are identified by a unique ID. Each change is signed with a cryptologic hash (SHA-1) 
which contains the datetime stamp, username, source location and other metadata for tracking and security. Permissions to view, update, and pull repositories 
can be set to provide additional control over the workflow.

[Top](#table-of-contents)

---
## How to use this Guide
This guide is designed to be both a quick reference, as well as a tutorial on common (and unique) situations and best practice. For simplicity, this guide 
will be broken down in the following sections:
-	**Quick Reference** – A “cheat sheet” to common commands (CLI command format) used in Git, with links to additional guidance for Git commands. 
-	**Getting Started** – This section contains “how-to” scenarios on setting up Git for your workspace or IDE. 
-	**Workflow** – Outline the MACPro workflow for DevOps team
-	**Branching Strategy** – MACPro automation Branching Strategy using Git 
-	**Use Cases** – Use cases in Frequently Asked Questions (FAQ) format. 
-	**Additional Links** 

[Top](#table-of-contents)

---
## Quick Reference
| Topic/Description |Command	
|:---|:---:|
|***Getting &amp; Creating Repositories***|
|Initialize a local Git repository|`git init`|
|Create a local copy of a remote repository|`git clone ssh://git@github.com/[username]/[repository-name].git`||
|***Saving, Undo &amp; Snapshots***|
|Check status|`git status`|
|Add a file to the staging area|`git add [file-name.txt]`
|Add all new and changed files to the staging area|`git add -A`|
|Commit changes|`git commit -m "[commit message]"`|
|Remove a file (or folder)|`git rm -r [file-name.txt]`|
|***Branching &amp; Merging*** |
|List branches (the asterisk denotes the current branch)|`git branch`|
|List all branches (local and remote)|`git branch -a`|
|Create a new branch|`git branch [branch name]`|
|Delete a local branch|`git branch -d [branch name]`|
|Delete a remote branch|`git push origin --delete [branch Name]`|
|Create a new branch and switch to it|`git checkout -b [branch name]`|
|Clone a remote branch and switch to it|`git checkout -b [branch name] origin/[branch name]`|
|Switch to a branch|`git checkout [branch name]`|
|Switch to the branch last checked out|`git checkout -` |
|Discard changes to a file |`git checkout -- [file-name.txt]`|	
|Merge a branch into the active branch|`git merge [branch name]`|
|Merge a branch into a target branch|`git merge [source branch] [target branch]`|
|Stash changes in a dirty working directory|`git stash`|
|Remove all stashed entries|`git stash clear`||
|***Sharing &amp; Updating Projects***|
|Push a branch to your remote repository |`git push origin [branch name]`|
|Push changes to remote repository (and remember the branch) |`git push -u origin [branch name]`|
|Push changes to remote repository (remembered branch)	|`git push`|
|Delete a remote branch|`git push origin --delete [branch name] `
|Update local repository to the newest commit |`git pull`
|Pull changes from remote repository|`git pull origin [branch name]`
|Add a remote repository|`git remote add origin ssh://git@github.com/[username]/[repository-name].git`
Set a repository's origin branch to SSH	|`git remote set-URL origin ssh://git@github.com/[username]/[repository-name].git`
|***Inspection &amp; Comparison***
|View changes|	`git log`  	|
|View changes (detailed) 	| `git log --summary`
|Preview changes before merging |	`git diff [source branch] [target branch]`	

[Top](#table-of-contents)

## Getting Started

This section is intended to provide information about how to set up git from IDEs (Eclipse,PyCharm), plugins (Egit, Git plugin for PyCharm) or the desktop instance (GitHub, web UI).

[Top](#table-of-contents)

##	MACPro DevOps Automation Workflow 

### Overview

To get an understanding of the DevOps Automation Workflow for MACPro, understand the following diagram:
[DIAGRAM]

Scripts and Script workflow contains the code for executing a specific action(s).  Playbook workflows contain the script(s) for executing a given test case. The Driver workflows execute all the playbooks that are mapped to it.  

###	Creating new automation Driver workflow

*Steps to create*
1.	When creating/using new users for a workflow, ensure that all the users for Macpro uses same password

2.	When creating a new workflow/function/file, adding comments would be really helpful.

##	Branching Strategy using Git
###	Overview

In order to have Git support the overall goals of the DevOps Automation team, the branching strategy provided will show the evolution of a task. The goal is to have Git reflect a consistent timeline from start to finish and best facilitate team member collaboration to complete a task.
###	Setting up Local Repository

Whenever starting any new work, first create a local working directory in your workspace. This is where your local repository will sync with the remote repository. Navigate to the workspace directory and perform a git init, to setup the local repository. ``git clone <url>`` to download the remote repository to your local repository in the workspace you created. You can make then make any changes locally. 
### Branching

Think of a branch(es) as a representation of a workflow; whether it is a project, sub-project, sprint task/story –or a combination of workflows. The base or “master” branch is the “root” of the tree. This could be baseline, or the last complete project or release. The “master” branch is implied to be in production or ready for product release. Typically, all other work flows start from the “master”
###	Branch Scope

A team member's role in the project will determine the branch scope and access within the remote repository tree structure. This will shape the git commands available to perform tasks work scope. An example of branching scope could be broken down like this:

```mermaid
graph LR

r2((Hotfix)) --> r1
r1t1((Task #1)) --> r1
r1t2((Task #2)) --> r1
r1t3((Task #3)) --> r1
r2t1((Bug #1)) --> r2
r2t2((Bug #2)) --> r2
r1((Release)) --> p1((Project))

```

Based on the team member's role, they may only have be able to "request" to perfom actions(push or merge) on the remote repository. Of course, a developer will have the greatest flexibility within the local repo. More can be found in the GitLab Documentation, under [Permissions](https://docs.gitlab.com/ee/user/permissions.html) . Here is an example of the access and commands needed, based on role(from greatest to least restriction):


|Job Title|Commands Required|Git Access Role|
|---|---|---|
|Developer|`push`,`pull`,`clone`|Developer|
|Team Lead/Lead Developer|`merge`,approve merge requests,create new remote branch|Maintainer
|Project/Program Manager/Lead Architect|`delete branch`,create namespace|Owner|

If a team is assigned to a particular task, they would checkout the corresponding branch using `git checkout -b <branch name>`. The `-b` options creates a new local branch, if it is not in your local repository. If the branch name exists in your local repository, git will ignore it the `-b` option and `checkout`. Team members can work independently on their local repository, pushing changes to the remote branch as if it were the 'master' branch. In their local repository, the individual teams can make changes, commit, and push the finished work to the remote branch. Team members can also create additional sub-branches to the team branch, or `fork` the team branch into a user remote branch, depending on the situation. 

Once the team leader, developer, or whomever is responsible for verifying work in that branch is complete, they will merge (or merge request) all changes (including sub-branches) into that `master` branch or next level branch. The heirarchy continues until the owner of the project merges all branches into the `master` branch.

### Creating Branches (Local and Remote)

There are two ways to create a branch; from the remote repository or your local repository. To create a new branch remotely, navigate to the remote repository and create it from the web UI. Then, from your local repository, perform a git pull to retrieve the updated repository. You can also perform a git fetch -v to see all branches (include new ones from the last pull/clone) available to checkout without affecting your local repository. After you pull from the remote repository, perform a git checkout -b <branch> to check-out the local branch (if does not exist locally, -b option will create it). You will then be automatically navigated to that branch and work within that branch.
You can also create the branch from their local repository.  `git branch <branch name>` creates a new local branch. When you are ready to push to the remote repository, perform a git push -u <”origin” or remote_branch> <your_local_branch> and that will create a new remote branch of your local branch and all changes.

###	Branch Commit, Push, and Merge
Work may be saved locally; however, it is considered “unstaged” by Git. In order to ensure that your changes are recognized, the changes must be “staged”. To do this, perform a `git add <directory_or_file>`. Wildcards are accepted and it is recursive, so a `git add` to the base of a directory will commit everything within. Using `-p` option allows you to chose what to commit. git remove performs the opposite of addition. 
To save to the local branch, `git commit -m <comments> <branch>`. When ready to push to the remote, you can either `git merge <local_master>` or `git push <remote_branch> <local_branch>` 
After merging the branch into master, delete the branch. Git tracks all commits and merges with identifier IDs. If the team would like to keep track record of prior releases, for example -– to roll back to a previous release, assign a git tag to that event. Tagging will be further explained in the documentation . Additional example scenarios using `git tag` can be found in the [Use Case](#use-case) section. 


###	Deleting Branches (Local and remote)

To delete a branch locally, you cannot be checked into the intended branch. Perform a `git branch -d <branch>` to delete the branch. It is recommended that all merge actions and commits are pushed, prior to deletion. Another best practice is to tag the project state before the deletion. Further explanation provided in the [Branch Tag](#git-tag) section.
	
To delete a remote branch, perform a git push branch --delete <remote_branch>. 
###	Branch Tag

Tag references are a way for the team to quickly reference the project state, commit, or merge, in the timeline. 
This is useful if you need to refer or revert to a previous state, after a branch has been deleted. 
This reduces metadata clutter and overhead, improving efficiency, while providing security and tracking to stakeholders. 

##	Git Workflow

A typical Git workflow diagram:
![fig1](img/fig1.png)
Example 1 (Ex.1) shows the `master` branch as the main workflow. A planned task or tasks (in blue) for a release cycle are named in the `Release` branch workflow, by naming convention.  Commits and pushes to the branch (the black diamonds) move the timeline or head of the branch forward. When a merge or merge request is performed (Ex.2), the `Release` branch merges all the delta changes into the `master` branch. There is another branch (in yellow) for hot fixes, bug fixes and patches, which has a different workflow. While planned tasks/features/stories are tracked, unplanned events need to be tracked as well. These unplanned tasks could potentially impact a sprint cycle if not properly managed.

 Example 3 (Ex. 3) shows one solution to managing this risk. In this scenario, the hotfix has been tested and ready to be merged. Note the relationship and coordination between the two branches, as neither the release branch nor the ‘hotflix’ branch development progress directly impacts the other branch. The ‘hotfix’ branch will “rebase” or re-baseline with the other branch, based on the development changes. This ensures proper synchronization within a release cycle. Normally, this will occur before the release date and the branches are merged and deleted. However, if the branches conflict or the merge is postponed, the ‘hot fix’ branch is not deleted, but it moves into the next release cycle.

Example 4 (Ex. 4) shows the other solution, if the ‘hotfix’ that was previously not ready for merging, but is ready for next release cycle. Assuming a successful merge, this branch will merge into the next release branch. The point in the “release” branch (in purple) could be merged into the `master`, if approved. However, it is typically merged within the next release as part of the backlog. This method prevents further delays and backlogs.

## Use Cases

###	Syncing Changes with Team



#### Scenario: 
Developer A and Developer B, on the same project, check out the `master` branch at the start in the sprint. Developer A makes a change and performs a git push 
to the  `master` or `branch`. Developer B, as they make their own changes and wants to incorporate those updates to their current workflow.

#### Solution: 
Developer B performs a git pull `--rebase` on the `master` branch.
---

![fig2](img/fig2.png)

Explanation: In the above example, Team A (in blue) and Team B (in yellow), pulled from the master (in green) at the same time. At that moment, their projects were in sync. Team A and Team B are working on their assignments. In a traditional workflow, teams would not work asynchronously; they would have to wait for the code to be checked-in or some other solution to resync the workflow.


Even within Git, to manually resolve this issue, you would do the following here:
`git fetch --all`

`git stash <some temp branch>`

`git pull -–all`

`git checkout`

`git diff <temp branch> <new_branch>`

`git merge`

![fig3](img/fig3.png)
When a git pull is performed on the local repository without a `--rebase`, the pulls the `master` and `sub-branch` at the current state of the remote repository. Meaning, if the work is not stashed `git stash`, the local branch workspace will be overwritten by the remote branch. A common misstep is to perform a `git commit` prior to a `git pull`, attempting to save the development work. However, the local branch commits will still get overwritten by a remote pull. Here is a diagram to conceptualize a `git pull --rebase`. See below:

Instead of performing all these steps, it is more efficient to perform a `git pull –-rebase`!

Step 1 – Team Yellow reverts to a time in the project where both Team Blue and Team Yellow were in sync. 

Step 2 – Team Yellow add the changes from Team Blue to its project. Now the teams are in sync.

Step 3 – Team Yellow applies the delta changes made in their project to the updated project.

You may notice that the projects are still out of sync. However, this will be resolved here:

![fig4](img/fig4.png)

Assuming Team Blue made no changes, Team Yellow will send a git push to Team Blue branch and move Team Blue workflow forward in sync. 
Team Yellow could also send a merge request to Team Blue and merge the two workflows and achieve the same result. 
Any Team Blue changes since the last pull would have to perform another re-base.

###	Best Practices for Git Repo "Housekeeping"

#### Question:
Why is it best practice to tag the merge into and delete the branch; how can the Git repo track future history and efficiency?

#### Answer: 
It is more efficient, after merging, to tag the merge and delete the branch, for 2 reasons:

1. Git change history is always preserved within the metadata, regardless of the branch existence.
2. Improves performance of the repository.


#### Explanation: 
Since the log 
## References
